﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Nta.Models
{
	public class Story
	{
		public string Url   { get; set; }
		public string By    { get; set; }
		public string Title { get; set; }

		[JsonExtensionData]
		public Dictionary<string, object> ExtensionData { get; set; }
	}
}
