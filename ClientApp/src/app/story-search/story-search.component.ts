import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-story-search',
  templateUrl: './story-search.component.html',
  styleUrls: ['./story-search.component.css']
})

export class StorySearchComponent {
  url: string;
  http: HttpClient;
  term = new FormControl('');

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this.http = http;
    this.url = baseUrl + 'v0/api/stories/search/';
  }

  onSubmit() {
    // this.http.get(this.url + this.term.value)
    const url = 'https://hn.algolia.com/?q=' + this.term.value;
    window.location.href = url;
  }
}
