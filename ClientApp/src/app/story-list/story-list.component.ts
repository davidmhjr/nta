import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Story } from '../story';

@Component({
  selector: 'app-story-list',
  templateUrl: './story-list.component.html',
  styleUrls: ['./story-list.component.css']
})

export class StoryListComponent {
  public stories: Story[];

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    http.get<Story[]>(baseUrl + 'v0/api/stories').subscribe(result => {
      this.stories = result;
    }, error => console.error(error));
  }
}
