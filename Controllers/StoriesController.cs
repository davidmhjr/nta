﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Nta.Models;
using Nta.Services;
using Nta.Services.NewsServices;

namespace Nta.Controllers
{
	[ApiController]
	[Route("v0/api/[controller]")]
	public class StoriesController : ControllerBase
	{
		private readonly HackerNewsListService _newsService;
		private readonly string _cacheKey = "_stories";
		private IMemoryCache _cache;

		public StoriesController(HackerNewsListService newsService, IMemoryCache cache)
		{
			_newsService = newsService;
			_cache = cache;
		}

		// GET: v0/api/stories
		[HttpGet]
		public async Task<IEnumerable<Story>> Get()
		{
			//var data = System.IO.File.ReadAllText("Data/stories.json");
			//var stories = JsonSerializer.Deserialize<IEnumerable<Story>>(data);

			IEnumerable<Story> stories;

			// Look for cache key.
			if (!_cache.TryGetValue(_cacheKey, out stories))
			{
				// Key not in cache, so get data.
				stories = await _newsService.GetStoriesAsync();

				// Set cache options.
				var cacheEntryOptions = new MemoryCacheEntryOptions()
					.SetSlidingExpiration(TimeSpan.FromSeconds(1000));

				// Save data in cache.
				_cache.Set(_cacheKey, stories, cacheEntryOptions);

			}

			return stories;
		}

		// GET: v0/api/stories/search/{term}
		[HttpGet("search/{string}")]
		public RedirectResult Search(string term)
		{
			// Maybe send an IFrame with this page embedded back to the browser?
			// Maybe there's a search API endpoint I missed? IDK...
			return Redirect("https://hn.algolia.com/?q=" + term);
		}

	}
}
