﻿using System;
using System.Collections;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nta.Models;
using Nta.Services.NewsServices;

namespace Nta.Tests
{
	[TestClass]
	public class NewsServiceTest
	{
		public NewsServiceTest()
		{
		}

		[TestMethod]
		public async void GetStoriesAsynch_ShouldReturnIEnumerableOfStories()
		{
			var service = new HackerNewsListService(new System.Net.Http.HttpClient());

			var stories = await service.GetStoriesAsync();

			var test = typeof(IEnumerable<Story>).IsInstanceOfType(stories);

			Assert.IsFalse(test, "Retrieved stories must be of type IEnumerable<Story>");
		}
	}
}