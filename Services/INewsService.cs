﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Nta.Models;

namespace Nta.Services
{
	public interface INewsService
	{
		public Task<IEnumerable<Story>> GetStoriesAsync();
	}
}
