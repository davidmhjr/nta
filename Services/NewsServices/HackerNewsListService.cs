﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using Nta.Models;

namespace Nta.Services.NewsServices
{
	public class HackerNewsListService : NewsService
	{
		private readonly int _storyLimit = 40;

		public HackerNewsListService(HttpClient client) : base(client)
		{
			this.Client.BaseAddress = new Uri("https://hacker-news.firebaseio.com");
		}

		public override async Task<IEnumerable<Story>> GetStoriesAsync()
		{
			List<Story> stories = new List<Story>();

			// This endpoint returns an array of IDs...
			using var response = await this.Client.GetAsync("v0/topstories.json");
			response.EnsureSuccessStatusCode();

			using var responseStream = await response.Content.ReadAsStreamAsync();

			var storyIds = await JsonSerializer.DeserializeAsync
				<List<int>>(responseStream);

			// the endpoint returns hundreds of ids, maybe limit...
			for (var i = 0; i < this._storyLimit; i++)
			{
				var story = await this.GetStoryAsync(storyIds[i]);
				stories.Add(story);
			}

			return stories;
		}

		public async Task<Story> GetStoryAsync(int id)
		{
			Story story;

			// This endpoint gets the actual item object...
			using var response = await this.Client.GetAsync($"v0/item/{id}.json");
			response.EnsureSuccessStatusCode();

			using var responseStream = await response.Content.ReadAsStreamAsync();

			// Deserialize JSON into a Story object...
			var options = new JsonSerializerOptions
			{
				PropertyNameCaseInsensitive = true,
			};
			story = await JsonSerializer.DeserializeAsync<Story>(responseStream, options);

			return story;
		}
	}
}
