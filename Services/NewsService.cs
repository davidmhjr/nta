﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Nta.Models;

namespace Nta.Services
{
	public abstract class NewsService : HttpService, INewsService
	{
		public NewsService(HttpClient client) : base(client) { }

		public abstract Task<IEnumerable<Story>> GetStoriesAsync();
	}
}
