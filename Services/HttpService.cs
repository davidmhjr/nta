﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Nta.Services
{
	public abstract class HttpService
	{
		public HttpClient Client { get; }

		public HttpService(HttpClient client)
		{
			this.Client = client;
		}

		protected async Task<System.IO.Stream> getResponse(string path)
		{
			var response = await this.Client.GetAsync(path);
			response.EnsureSuccessStatusCode();

			using var responseStream = await response.Content.ReadAsStreamAsync();

			return responseStream;
		}

		public static bool TestForWebConnection()
		{
			// https://stackoverflow.com/questions/2031824/what-is-the-best-way-to-check-for-internet-connectivity-using-net
			try
			{
				using var client = new WebClient();
					using (client.OpenRead("http://google.com")) 
						return true; 
			}
			catch
			{
				return false;
			}
		}
	}
}